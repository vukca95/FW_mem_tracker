# FW Mem Tracker

## Description
I noticed that some projects are almost using all of the available memory. So, to make it easier to track and take care of I wanted to make a script that you can run and will update a table with the latest bin size.

This way the progress of memory consumption can be monitored in some way, ie Jenkins job.

## Installation
You need Python 3. Specifically, on 11-Sept-2021 when the repo is created and script started I used 3.8.10.

## Usage
python3 FW_mem_tracker.py -h

## Support
If you upgraded/expanded the code make a merge request and it will be reviewed. :)

## Roadmap
None at this point

## Contributions
Open to contributions

## License
Use it, don't lie from where you got it. And contribute if you have cool ideas, or something for example Jenkins related

## Project status
If needed it will be updated

