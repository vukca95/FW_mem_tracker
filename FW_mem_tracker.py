import sys
import os
import datetime
import argparse
import csv
from matplotlib import pyplot as plt

parser = argparse.ArgumentParser(description= "Save current file size to CSV")
requiredArguments = parser.add_argument_group("Required argument(s)")
requiredArguments.add_argument("-c", "--csvfile", type=str, help="Path to CSV in which to write the size", required=True)

exclusiveArguments = parser.add_mutually_exclusive_group()
exclusiveArguments.add_argument("-p", "--plot", type=str, help="Save the plot to a location")
exclusiveArguments.add_argument("-s", "--show", default=False, action='store_true', help="Show the plot in a window")

parser.add_argument("-b", "--binfile", type=str, default = '', nargs = '?', help="If location is provided saves the size to the CSV file")
parser.add_argument("-mm", "--maxmemory", type=int, default = 0, nargs = '?', help="Add a line to the plot representing max memory of the device")
parser.add_argument("-x", "--xaxis", type=str, help="If present saves this string instead of datetime string for x axis")

args = parser.parse_args()

if args.binfile:
	if not args.xaxis:
		now = datetime.datetime.now()
		xAxisVal = now.strftime("%d-%b-%Y\n%H:%M:%S")
	else:
		xAxisVal = args.xaxis
	data = [xAxisVal, os.path.getsize(args.binfile), "bytes"]
	with open(args.csvfile, 'a') as f:
		writer = csv.writer(f)
		writer.writerow(data)
	# If last binfile was higher than the max mem mark make fail signal file
	if (args.maxmemory != 0) and (os.path.getsize(args.binfile) > args.maxmemory):
		with open("OverMaxMem__" + os.path.basename(args.binfile) + "__.txt", 'w') as ff:
			ff.write(now.strftime("%d-%b-%Y\n%H:%M:%S\n"))

x = []
y = []
max_value_x = 0
with open(args.csvfile,'r') as f:
	plots = csv.reader(f, delimiter = ',')
	for row in plots:
		x.append(row[0])
		y.append(int(row[1]))
		max_value_x = max([int(row[1]), max_value_x])

if args.binfile:
	titleString = "Evolution of \"" + os.path.basename(args.binfile) + "\" size"
else:
	titleString = "Evolution of file size saved in \"" + os.path.basename(args.csvfile) + "\""
plt.plot(x, y, color = 'g', label='Size history')
plt.ylabel('Size (bytes)')
plt.title(titleString)
plt.grid()
if args.maxmemory != 0:
	mm = []
	for i in range(len(x)):
		mm.append(args.maxmemory)
	plt.plot(x, mm, color = 'r', label='Size ceiling')
highWaterMark = []
for i in range(len(x)):
	highWaterMark.append(max_value_x)
plt.plot(x, highWaterMark, color = 'b', label='High water mark')
plt.legend()
if args.plot:
	plt.savefig(args.plot, format = 'pdf')
if args.show is True:
	plt.show()

